<?php
return [    'name'=>'name',
            'details'=>'details',
            'price'=>'price',
    		'company'=>'NameCompany',
    		'contact_person'=>'contact_person',
    		'email'=>'email',
    		'phone'=>'phone',
    		'address'=>'address',
    		'city'=>'city',
    		'state'=>'state',
    		'postal_code'=>'postal_code',
    		'country'=>'country',
    	];