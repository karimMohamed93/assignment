@extends('index')    
@section('content')


<a href="{{url('/')}}" class="btn btn-primary">back</a>

<table border="1" cellpadding="1" cellspacing="1">
                    <tr>
                        <th>date</th>
                        <th>customer_id</th>
                        <th>order_discount</th>
                        <th>Recurring</th>
                        <th>billing_company</th>
                        <th>order_tax</th>
                        <th>shipping</th>
                        <th>status</th>
                        <th>quantity</th>
                        <th>product_id</th>
                    </tr>
                    
                    @foreach($saleses as $sale)
                    <tr>
                         
                         <td>{{$sale->date}}</td>
                         <td>{{$sale->customer_id()->first()->company}}</td>
                         <td>{{$sale->order_discount}}</td>
                         <td>{{$sale->Recurring}}</td>
                         <td>{{$sale->billing_company}}</td>
                         <td>{{$sale->order_tax}}</td>
                         <td>{{$sale->shipping}}</td>
                         <td>{{$sale->status}}</td>
                         <td>{{$sale->quantity}}</td>
                         <td>{{$sale->product_id()->first()->name}}</td>
                         
                       
                         
                    </tr>
                    
                    @endforeach
                    
                </table>

                  <a href="AddSale/create" class="btn btn-info" role="button">AddSale</a>
@endsection