@extends('index')    
@section('content')
<a href="{{url('AddProducts')}}" class="btn btn-primary">back</a>
<div class="box-header">
    <h3 class="box-title"> <?php echo 'AddProduct'; ?> </h3>
</div>
<hr>


 {!! Form::open(['url'=>'AddProducts']) !!}
     {!! Form::text('name',old('name'),['placeholder'=>'name','class'=>'form-control']) !!}
          <br>
    {!! Form::text('details',old('details'),['placeholder'=>'details','class'=>'form-control']) !!}
          <br>
     {!! Form::text('price',old('price'),['placeholder'=>'price','class'=>'form-control']) !!}
          <br>
          
          {!! Form::submit('AddProduct',['class'=>'btn btn-primary']) !!}

        {!! Form::close() !!}

@endsection


