@extends('index')    
@section('content')
<a href="{{url('AddSale')}}" class="btn btn-primary">back</a>
<div class="box-header">
    <h3 class="box-title"> <?php echo 'AddSale'; ?> </h3>
</div>
<hr>


 {!! Form::open(['url'=>'AddSale']) !!}

     <div class="form-group">
        {!! Form::label('date','date') !!}
        {!! Form::date('date',old('date'),['placeholder'=>'date','class'=>'form-control']) !!}
      </div>
          <br>
      <div class="form-group">
        {!! Form::label('customer','customer') !!}
        {!! Form::select('customer_id',App\Customer::pluck('company','id'),old('customer_id'),['placeholder'=>'customer','class'=>'form-control']) !!}
      </div>
      	<br>
      <div class="form-group">
        {!! Form::label('order_discount','order_discount') !!}
        {!! Form::text('order_discount',old('order_discount'),['placeholder'=>'order_discount','class'=>'form-control']) !!}
      </div>
          <br>
      <div class="form-group">
        {!! Form::label('order_tax','order_tax') !!}
        {!! Form::text('order_tax',old('order_tax'),['placeholder'=>'order_tax','class'=>'form-control']) !!}
      </div>
      <br>
      <div class="form-group">
        {!! Form::label('Recurring','Recurring') !!}
        {!! Form::select('Recurring',['yes'=>'yes','no'=>'no'],old('Recurring'),['class'=>'form-control']) !!}
     </div>
          <br>
     <div class="form-group">
        {!! Form::label('billing_company','billing_company') !!}
        {!! Form::text('billing_company',old('billing_company'),['placeholder'=>'billing_company','class'=>'form-control']) !!}
     </div>
     	<br>
     <div class="form-group">
        {!! Form::label('shipping','shipping') !!}
        {!! Form::text('shipping',old('shipping'),['placeholder'=>'shipping','class'=>'form-control']) !!}
     </div>
     <br>
     <div class="form-group">
        {!! Form::label('status','status') !!}
        {!! Form::select('status',['pending'=>'pending','refused'=>'refused','active'=>'active'],old('status'),['class'=>'form-control']) !!}
     </div>
     <br>
     <div class="form-group">
        {!! Form::label('quantity','quantity') !!}
        {!! Form::text('quantity',old('quantity'),['placeholder'=>'quantity','class'=>'form-control']) !!}
     </div>
     <br>
     <div class="form-group">
        {!! Form::label('product_id','product_id') !!}
        {!! Form::select('product_id',App\Product::pluck('name','id'),old('product_id'),['placeholder'=>'product_name','class'=>'form-control']) !!}
      </div>
          
          {!! Form::submit('AddProduct',['class'=>'btn btn-primary']) !!}

 {!! Form::close() !!}






@endsection


