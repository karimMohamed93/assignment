@extends('index')    
@section('content')

<a href="{{url('AddCustomers')}}" class="btn btn-primary">back</a>
<div class="box-header">
    <h3 class="box-title"> <?php echo 'AddCustomer'; ?> </h3>
</div>
<hr>
<div class="box-body">
    {!! Form::open(['url'=>'AddCustomers']) !!}
      <div class="form-group">
        {!! Form::label('company','company') !!}
        {!! Form::text('company',old('company'),['placeholder'=>'company','class'=>'form-control']) !!}
      </div>
        <br>
      <div class="form-group">
        {!! Form::label('contact_person','contact_person') !!}
        {!! Form::text('contact_person',old('contact_person'),['placeholder'=>'contact_person','class'=>'form-control']) !!}
      </div>
        <br>
      <div class="form-group">
        {!! Form::label('email','email') !!}
        {!! Form::text('email',old('email'),['placeholder'=>'email','class'=>'form-control']) !!}
      </div>
        <br>
      <div class="form-group">
          {!! Form::label('phone','phone') !!}
          {!! Form::text('phone',old('phone'),['placeholder'=>'phone','class'=>'form-control']) !!}
      </div>
          <br>
      <div class="form-group">
        {!! Form::label('address','address') !!}
        {!! Form::text('address',old('address'),['placeholder'=>'address','class'=>'form-control']) !!}
      </div>
        <br>
      <div class="form-group">
          {!! Form::label('city','city') !!}
          {!! Form::text('city',old('city'),['placeholder'=>'city','class'=>'form-control']) !!}
      </div>
          <br>
      <div class="form-group">
        {!! Form::label('state','state') !!}
        {!! Form::text('state',old('state'),['placeholder'=>'state','class'=>'form-control']) !!}
      </div>
        <br>
      <div class="form-group">
        {!! Form::label('postal_code','postal_code') !!}
        {!! Form::text('postal_code',old('postal_code'),['placeholder'=>'postal_code','class'=>'form-control']) !!}
      </div>
      <br>
      <div class="form-group">
      {!! Form::label('country','country') !!}
      {!! Form::text('country',old('country'),['placeholder'=>'country','class'=>'form-control']) !!}
      </div>
      <br>
  
    {!! Form::submit('AddCustomer',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>

@endsection