<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;

class ControllerCustomer extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$customer = customer::orderBy('id','desc')->get();
        $cust = Customer::get();
        return view('layout.indexcustomer',['customers'=>$cust]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('layout.customeres');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //return request()->all();


        $attribute=[
            'company'=>trans('admin.company'),
            'contact_person'=>trans('admin.contact_person'),
            'email'=>trans('admin.email'),
            'phone'=>trans('admin.phone'),
            'address'=>trans('admin.address'),
            'city'=>trans('admin.city'),
            'state'=>trans('admin.state'),
            'postal_code'=>trans('admin.postal_code'),
            'country'=>trans('admin.country'),


        ];
        $data = $this->validate(request(), [
            'company'=>'required',
            'contact_person'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'postal_code'=>'required',
            'country'=>'required',
        ],[],$attribute);

        Customer::create($data);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
