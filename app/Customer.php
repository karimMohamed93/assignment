<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customers';
    protected $fillable = ['company','contact_person','email','phone','address','city','state','postal_code','country'];
}
