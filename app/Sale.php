<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    //
    protected $table = 'sales';
    protected $fillable = ['date','customer_id','order_discount','Recurring','billing_company','order_tax','shipping','status','quantity','product_id'];

    public function customer_id(){


    	return $this->hasOne('App\Customer','id','customer_id');

    }
    public function product_id(){


    	return $this->hasOne('App\Product','id','product_id');

    }
}
